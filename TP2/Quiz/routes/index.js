var HOME_PAGE = 'index';
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/:page', function(req, res, next) {
	var page = req.params.page;
	var splittedPath = page.split('.html')[0].toLowerCase();
	res.render(splittedPath+'.pug', { title: 'Express' });
	next();
});
module.exports = router;

