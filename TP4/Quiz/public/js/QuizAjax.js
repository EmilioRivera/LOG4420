(function(QuizAjax, $, undefined){
	QuizAjax.g = "Works";
	QuizAjax.something = function() { return "something"; }
	QuizAjax.getRandomQuestion = function(fCallback){
		$.ajax({
			url: "/api/question",
			contentType: "application/json; charset=utf-8",
			dataType: "text json",
			success: function(res){
				fCallback(res[0]);
			},
			error:function(err){
				alert("error:" + err);
			}
		});
	}
	
	QuizAjax.getQuestionByDomain = function(fCallback, domain, number){
		//var questionNumber = (number === undefined || isNaN(number)? 1 : number);
		$.ajax({
			url: "/api/question",
			data:{
				"domain": domain,
				"count": number
			},
			success: function(res){
				fCallback(res);
			},
			error: function(x,y,z){
				alert(x);
			}
		});
	}
	
	QuizAjax.verifyAnswer = function(fCallback, questionId, responseId){
		$.ajax({
			url: "/api/question/verify",
			data:{
				"questionid": questionId,
				"answerid": responseId
			},
			success: function(res){
				fCallback(res);
			},
			error: function(x,y,z){
				alert(x);
			}
		});
	}
	
	QuizAjax.getQuestionCount = function(fCallback, domain){
		$.ajax({
			url: "/api/question/count",
			data:{
				"domain": domain,
			},
			success: function(res){
				fCallback(res);
			},
			error: function(x,y,z){
				alert(x);
			}
		});
	}
	
	QuizAjax.startExam = function(fCallback, domain, count, username){
		$.ajax({
			url: "/api/exam",
			data:{
				"domain": domain,
				"count": count,
				"username": username,
			},
			success: function(res){
				fCallback(res);
			},
			error: function(x,y,z){
				alert(x);
			}
		});
	}
	
	QuizAjax.getUser = function(fCallback, userid){
		$.ajax({
			url: "/api/user",
			data:{
				"userid": userid,
			},
			success: function(res){
				fCallback(res);
			},
			error: function(x,y,z){
				alert(x);
			}
		});
	}
	
	QuizAjax.sendAnswer = function(fCallback, choice, username){
		$.ajax({
			url: "/api/exam/verify",
			type: "POST",
			data:{
				"choice": choice,
				"username": username
			},
			success: function(res){
				fCallback(res);
			},
			error: function(x,y,z){
				alert(x);
			}
		});
	}
	
	QuizAjax.deleteAllQuestions = function(fCallback){
		$.ajax({
			url: "/api/question/",
			type: "DELETE",
			success: function(res){
				fCallback(res);
			},
			error: function(x,y,z){
				alert(x);
			}
		});
	}
	
}(window.QuizAjax = window.QuizAjax || {}, jQuery));
