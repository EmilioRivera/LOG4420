function redirect(location){
	window.location = location;
}

function startTest(location)
{
	redirect(location);
}

function startExam(location){

	// read values from DOM
	var domain = $("#cat-select :selected").val();
	var number =  $("#exam-num").val();
    
	localStorage.setItem("domain-choice", domain);
	localStorage.setItem("number-choice", number);
	redirect(location);
}

var theQuestion;
var bgs = [
'http://i.imgur.com/MjBOmGgg.jpg',
'http://i.imgur.com/mQA0fIOg.jpg',
'http://i.imgur.com/CsI5hAig.jpg',
'http://i.imgur.com/byzTecq.jpg',
'http://i.imgur.com/byzTecqg.jpg',
'http://i.imgur.com/pErfJrWg.jpg',
'http://i.imgur.com/d8bR96Bg.jpg',
'http://i.imgur.com/MArLNIXg.jpg'];
var im;
var repeat;
var globalExamQuestions;
var examIndex = 0;
var cExamArray = [];
function getBG(){
	return  bgs[Math.floor(Math.random()*bgs.length)];
}

window.onload= function(){
    //var baseHREF= window.location.pathname + window.location.search;
    //document.querySelector('a[href$="'+ baseHREF+ '"]').className += ' current';
    //im=document.getElementById('mainImage');
    //im.src = getBG();
    //repeat = setInterval(function() {
    //	changeImage();
    //}, 5000);

};

function changeImage(){
	var tmp = getBG();
	while (tmp == im.src){
		tmp = getBG();
	}
	im.src = tmp;
}

function Identity(res){ console.log(res);}

function getRandomQuestion(fCallback)
{
	$.ajax({
		url: "/randomQuestion",
        contentType: "application/json; charset=utf-8",
        dataType: "text json",
		success: function(res){
			localStorage.setItem("QuestionObject", JSON.stringify(res));
			fCallback(res);
		},
		error:function(err){
			alert("error:" + err);
		}
	});
}

function getExamQuestions(fCallback, domain, number){

	$.ajax({
		url: "/examQuestions",
		data:{
			"domain": domain,
			"number": number
		},
		success: function(res){
			localStorage.setItem("ExamQuestions", JSON.stringify(res));
			fCallback();
		},
		error: function(x,y,z){
			alert(x);
		}
	});
}


var temp;
function LoadTestQuestion(questionObject)
{
	$(".answerDiv label").attr("draggable",true);
	localStorage.setItem("QuestionObject", JSON.stringify(questionObject));
	console.log(questionObject);
	temp = questionObject;
	var question = temp.question;
	$('#question').html(question);
	$('#domain').html(temp.domaine);
	var $fLabel = $("label[for=answer-0]");
	var $sLabel = $("label[for=answer-1]");
	var $tLabel = $("label[for=answer-2]");
	$fLabel.attr("answerid","rep0").html(temp.reponse[0]);
	$sLabel.attr("answerid","rep1").html(temp.reponse[1]);
	$tLabel.attr("answerid","rep2").html(temp.reponse[2]);

	$sLabel.on('dragstart', DragStart);
	$tLabel.on('dragstart', DragStart);
	$fLabel.on('dragstart', DragStart);

}


function LoadExamQuestion(index)
{
	$(".answerDiv label").attr("draggable",true);
	var questions = JSON.parse(localStorage.getItem("ExamQuestions"));
	var question = questions[index];
	$('#question').html(question.question);
	$('h1').html(question.domaine);
	var $fLabel = $("label[for=answer-0]");
	var $sLabel = $("label[for=answer-1]");
	var $tLabel = $("label[for=answer-2]");
	$fLabel.attr("answerid","rep0").html(question.reponse[0]);
	$sLabel.attr("answerid","rep1").html(question.reponse[1]);
	$tLabel.attr("answerid","rep2").html(question.reponse[2]);

	$tLabel.on('dragstart', DragStart);
	$fLabel.on('dragstart', DragStart);

}

function DragStart (event) {
	var answerid = $(event.target).attr('answerid');
	// originalEvent is used because dataTransfer belongs to the HTML
	// event, not jQuery's. 
	event.originalEvent.dataTransfer.setData("answerid", answerid);
}

function VerifyAnswer(event, answerid){
	var $src = $(".answerDiv label[answerid=" + answerid + "]");
	console.log('answer selected : ' + answerid);
	var questionObject = GetLocalStorageObject("QuestionObject");
	var repIndex = answerid.split('rep')[1];
	console.log('QID: ' + questionObject._id);
	clearAnswersCSS();
	
	-
	QuizAjax.verifyAnswer(function(isGoodAnswer){
		if(isGoodAnswer){
			alert('YOU DEFEATED');
			$src.closest("div").addClass("goodAnswer").removeClass("badAnswer");
			totalRandomSucces = totalRandomSucces + 1;
			localStorage.setItem("totalRandomSucces",totalRandomSucces);
		} else{
			alert('YOU LOSE');
			$src.closest("div").addClass("badAnswer").removeClass("goodAnswer");
		}
		
	}, questionObject._id, repIndex);
	
	$(".answerDiv label").attr("draggable",false);
	
	
	
	return;
	
	var totalRandomExams = parseInt(localStorage.getItem("totalRandomExams")); // total random exams done
	totalRandomExams = isNaN(totalRandomExams)? 1 : totalRandomExams + 1;

	var totalRandomSucces = parseInt(localStorage.getItem('totalRandomSucces')); // total succesful random exams answered
	totalRandomSucces = isNaN(totalRandomSucces)? 0 : totalRandomSucces;
	localStorage.setItem("totalRandomExams",totalRandomExams);
	if (answerid == questionObject.bonneReponse){
		$src.closest("div").addClass("goodAnswer").removeClass("badAnswer");
		totalRandomSucces = totalRandomSucces + 1;
		localStorage.setItem("totalRandomSucces",totalRandomSucces);
	} else {
		$src.closest("div").addClass("badAnswer").removeClass("goodAnswer");
	}
	$(".answerDiv label").attr("draggable",false);
	UpdateStatistics();

}

function VerifyExamAnswer(event, answerid){
	
	// Contact the server to notify of our choice
	
	var choice = answerid.split('rep')[1];
	console.log('choice index: ' + choice);
	if (isNaN(choice)){
		alert('Error while getting index of answer');
		return;
	}
	QuizAjax.sendAnswer(function(isGoodAnswer){
		if (isGoodAnswer) alert('YOU WIN');
		else alert('YOU LOSE');
	},choice);
	
	var question = JSON.parse(localStorage.getItem("ExamQuestions"));
	var currentIndex = parseInt(localStorage.getItem("examIndex"));
	return question[currentIndex].bonneReponse == answerid;
}

function NextQuestion(){
	clearAnswersCSS();
	$(".answerDiv input").attr("checked", false); // Remove the radio button selection
	//getRandomQuestion(LoadTestQuestion);
	QuizAjax.getRandomQuestion(LoadTestQuestion);
}

function NextExamQuestion(){
	clearAnswersCSS();

	var idx = parseInt(localStorage.getItem("examIndex"));
	var questionNumbers = parseInt(localStorage.getItem("number-choice"));
	$(".answerDiv input").attr("checked", false); // Remove the radio button selection
	if(idx == questionNumbers){
		redirect('examscore');
	}
	else{
		localStorage.setItem("examIndex", idx + 1);
		LoadExamQuestion(idx + 1);
	}
}

function clearAnswersCSS(){
	$(".answerDiv div").removeClass("goodAnswer badAnswer");
}

function GetLocalStorageObject(key){
	return JSON.parse(localStorage.getItem(key));
}

function OnDrop(event){
	var value = event.dataTransfer.getData("answerid");	
	VerifyAnswer(event, value);
}

function OnDropExam(event){
	var value = event.dataTransfer.getData("answerid");	
	var isGoodAnswer = VerifyExamAnswer(event,value);
	var currentIndex = parseInt(localStorage.getItem("examIndex"));
	if (isGoodAnswer){
		alert("You win !");
		cExamArray[currentIndex] = 1;
	} else {
		cExamArray[currentIndex] = -1;
	}
}

function allowDrop(event){
	event.preventDefault();
}

// Updates values of the mainTable
function UpdateStatistics(){
	var randSucces = localStorage.getItem("totalRandomSucces");
	randSucces = randSucces ? randSucces : "0";
	var randTries = localStorage.getItem("totalRandomExams");
	randTries = randTries ? randTries :"0";
	$("#randomStatsValue").html(randSucces + " / " + randTries);
}

function UpdateExamStats(userObject){
	var allResults = userObject.exams;
	$("#examBody").html("");
	allResults.forEach(function(s){
		console.log(s);
		var sub = s.reduce(function(tot, c){return tot + c.score;},0);
		var row ='<tr>';
		row+= ('<td> Exam( ' + s[0].domain + ' )</td>');
		row+= ('<td>' + sub + " / " + s.length + '</td>');
		row+='</tr>';
		$("#examBody").append(row);
	});
}


// Deletes everything in localStorage
function ClearLocalStorage()
{
	localStorage.clear();
	UpdateStatistics();
}

function LoadUserStats(userObject){
	//console.log(userObject);
	//if (userObject.exam_in_progress){
	//	var $exambtn = $("#exam-btn");
	//	var idx = userObject.exam_index;
	//	localStorage.setItem("ExamIndex", idx);
	//	$exambtn.text('Reprendre examen');
	//	
	//	// TODO : Change the on click
	//	$exambtn.off();
	//	$exambtn.on('click', function(){
	//		alert("Reprendre examen à l'index : " + idx);
	//		redirect('/api/exampage');
	//	});
	//}
	
	// Load the exam stats
	var totalNumberExams = userObject.exams.length;
	var finishedExamsArray = (userObject.exam_in_progress ? userObject.exams.slice(0, -1) : userObject.exams);
	if (finishedExamsArray.length != 0){
		var sum = 0;
		finishedExamsArray.forEach(function(s){
			console.log(s);
			
			var sub = s.reduce(function(tot, c){return tot + c.score;},0);
					console.log(sub);
			sum += sub / s.length;
		});
		var fin = sum / totalNumberExams;
		var averageString = fin.toPrecision(4) * 100 + '%'
		$("#examStats").text(averageString);
	}
	$("#randomStatsValue").html(userObject.good_test + " / " + userObject.total_test);
}

function BindDashboardButtons(userObject){
	if (userObject.exam_in_progress){
		var $exambtn = $("#exam-btn");
		var idx = userObject.exam_index;
		localStorage.setItem("ExamIndex", idx);
		$exambtn.text('Reprendre examen');
		
		// TODO : Change the on click
		$exambtn.off();
		$exambtn.on('click', function(){
			alert("Reprendre examen à l'index : " + idx);
			redirect('/api/exampage');
		});
	}
}

function LoadQuestionCount(countObject){
	console.log(countObject);
	$("#cat-select option").each(function(){
		var $this = $(this);
		if (($this.val() == "-")) return;
		var count = countObject[$this.val()];
		$this.attr('data-tot', count);
		$this.text($this.text() +  " ("+count+")");
		console.log($this.val() + " and " + count);
	});
}

function SetSelectionMax(something){
	var $selectedOp = $("#cat-select option:selected");
	var max = $selectedOp.attr('data-tot');
	var $val = $("#exam-num").attr('max', max);
	if ($val.val() > max) { $val.val(max); }
	if ($val.val() < 0) {$val.val(0);}
}

function VerifyMax(e){
	var $this = $(this);
	var thisNumber = Number($this.val());
	var max = Number($this.attr('max'));
	if (isNaN($this.val())) {
		$this.val(0);
		e.preventDefault();
		return false;
	}
	
	if (thisNumber <= 0) $this.val(0);
	else if (thisNumber > max) $this.val(max);
	else return true;
	//if (thisNumber <= max && thisNumber >= 0) return true;
}

function startExamClick(){
	var selection = $("#cat-select").val();
	var number = $("#exam-num").val();
	var isValid = true;
	if (isNaN(number)){
		isValid = false;
	} 
	if (selection == "-"){
		isValid = false;
	}
	if (!isValid){
		return;
	}


	startAjaxExam(selection,number);
}

function startAjaxExam(d,n){
	var domain = d;
	var number = n;
	// Get values from API
	QuizAjax.startExam(function(result){
		console.log(result);
		localStorage.setItem("ExamQuestions", JSON.stringify(result));
		redirect('/api/exampage');
	}, domain, number);
}

function EmilioDrag(event){
	var aid = event.target.children.item('span').id;
	event.dataTransfer.setData("answerid", aid);	
}
function EmilioDrop(event){
	
	var aid = event.dataTransfer.getData('answerid');
	var answerIndex = aid.split('answer-')[1];
	console.log('index : ' + answerIndex);
	// Notify server
	
	QuizAjax.sendAnswer(function(isGoodAnswer){
		if (isGoodAnswer) alert('YOU WIN');
		else alert('YOU LOSE');
		redirect('/api/exampage');
	},answerIndex);
}
