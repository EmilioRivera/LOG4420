var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var db = require('../lib/db');
var mongoose = require( 'mongoose' );
var ObjectId = require('mongodb').ObjectId; 
var QuestionModel = mongoose.model('Question');
var formController = {};
formController.CreateQuestion = function(question, category, answer_choices, the_answer, fCallback){
    db_createQuestion("Harambe[harcoded]", question, category, answer_choices, the_answer, fCallback);
}

formController.RandomQuestion = function(fCallback){
    db_getRandomQuestion(fCallback);
}

// Gets questions based on the domain (if specified). Results
// repeat themselves
formController.Questions = function(fCallback, domain){
	var resultArray = [];
	if (domain === undefined || domain == null){
		QuestionModel.find(function(err,res){
			if(err) return console.error("err" + err);
			fCallback(err, res);
			return;
		});
	} else {
		QuestionModel.find({"domaine": domain}, function(err,res){
			if(err) return console.error("err" + err);
			fCallback(err, res);
			return;
		});
	}
}

formController.VerifyQuestion = function(fCallback, questionId, chosenAnswerIndex){
	try {
		var myObjectId = new ObjectId(questionId);
	} catch (ex){
		fCallback(ex, false);
		return;
	}
	console.log('server side objectid is ' + questionId);
	QuestionModel.findById(questionId, function(err, res){
		if(err) return console.error("err" + err);
		var isGoodAnswer = (res.bonneReponse == chosenAnswerIndex)
		fCallback(err,isGoodAnswer);
		return;
	});
}






formController.GetQuestionsByDomain = function(domain,fCallback){
    db_getQuestionByDomain(domain,fCallback);
}

function db_createQuestion(user_id, question, category, answer_choices, the_answer, fCallback){
    new QuestionModel({
        user_id: user_id,
        question: question,
        domaine: category,
        reponse: answer_choices,
        bonneReponse: the_answer
    }).save(function(err, product, count){
        fCallback(err, product, count);
    });
}

function db_getRandomQuestion(fCallback){

    QuestionModel.find(function (err, result) {
        if (err) return console.error(err);
        var randomQuestion = result[Math.floor(Math.random()*result.length)];
        randomQuestion.bonneReponse = -1;
        console.log(JSON.stringify(randomQuestion));
        fCallback(err,randomQuestion);
    });
}

function db_getQuestionByDomain(domain,number,fCallback){
    console.log("sent domain " + domain);
    QuestionModel.find({'domaine': domain}, function(err,result){
        if(err) return console.error("err" + err);
        console.log("res" + JSON.stringify(result) );
        result.forEach(function(x){delete x.bonneReponse});
        fCallback(err,result);
    });
}

module.exports = formController;
