var QuizCategories = {};
QuizCategories.Categories = [
	{value: 'js', fullName : 'JavaScript'},
	{value: 'css', fullName : 'CSS'},
	{value: 'html', fullName : 'HTML'}
];

QuizCategories.exist = function(categoryName){
	for(var i = 0; i < QuizCategories.Categories.length; ++i){
		if (QuizCategories.Categories[i].value === categoryName)
			return true;
	}
	return false;
}

module.exports = QuizCategories;
