var HOME_PAGE = 'index';
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var formController = require('../Controller/formcontroller');
var QuizCategories = require('../QuizCategories');

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

router.get('/randomQuestion', function(req,res,next){
	formController.RandomQuestion(function(error,result){
		res.json(result);
	});
});

router.get('/examQuestions',function(req,res,next){
	formController.GetQuestionsByDomain(req.query.domain,req.query.number,function(error,result){
		res.json(result);
	});
});

router.get('/:page', function(req, res, next) {
	var page = req.params.page;
	var splittedPath = page.split('.html')[0].toLowerCase();
	res.render(splittedPath+'.pug', { title: 'Express' });
	//anext();
});

router.post('/question', jsonParser, function(req, res, next){
	if (!req.body) return res.sendStatus(400);
	var theQuestion = req.body.question;
	console.log(req.body);
	var optionZero = req.body.text0;
	var optionOne = req.body.text1;
	var optionTwo = req.body.text2;
	console.log(optionZero);
	var category = req.body.category;
	var optionArray = [];
	var answer_index = req.body.answer_index;
	var validCategories = QuizCategories.Categories;
	
	// Validate
	var isValid = true;
	if (theQuestion == "") { isValid = false ;}
	if (answer_index < 0 || answer_index > 2 || answer_index == null) { isValid = false;}
	if (!(validCategories.filter(function(cat){return cat.value === category;}).length > 0)) {isValid = false;}
	
	if (!isValid){
		res.sendStatus(400);
		return;
	}
	console.log("answer index: " + answer_index);
	
	optionArray.push(optionZero);
	optionArray.push(optionOne);
	optionArray.push(optionTwo);
	console.log(optionArray);
	// The client won't procede until a response is provided, that is
	// until a res is set.
	
	
	formController.CreateQuestion(theQuestion, category, optionArray, answer_index, function( err, product, count){
		if (err != null){
			res.sendStatus(new Error("Error while doing something"));
			return;
		}
		console.log("My three parameters : ");
		console.log("First parameter: " + err);
		console.log("Second parameter: " + product);
		console.log("Third parameter: " + count);
		console.log('All is ok, saved to db');
		res.json({status: "200", objectCreated: product});
	});
	
});
module.exports = router;

