var HOME_PAGE = 'index';
var express = require('express');
var router = express.Router();
var controller = require('../Controller/controller');
/* GET home page. */
/* localhost/ajax/... */
router.get('/', function(req, res, next) {
	//TODO: Change this
  res.render('index', { title: 'Ajax!' });
});


router.get('/randomQuestion', function(req, res, next) {
	//TODO: Change this
	res.json(controller.GetRandomQuestion());
});

router.get('/examQuestions', function(req, res, next) {
	//TODO: Change this
	res.json(controller.GetQuestionByDomain(req.query.domain,req.query.number));
});

router.get('/:page', function(req, res, next) {
	var val = controller.TheVal;
	var other = controller.GetQuestion(2);
	var page = req.params.page	
	res.render('index', { title: "Value:"+ other });
	next();
});
module.exports = router;

