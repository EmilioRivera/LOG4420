function redirect(location){
	window.location = location;
}

function startTest(location)
{
	redirect(location);
}

function startExam(location){

	// read values from DOM
	var domain = $("#cat-select :selected").text();
	var number =  $("#exam-num").val();
    number = (number <= 4 && number >= 1) ? number : 4;
	localStorage.setItem("domain-choice", domain);
	localStorage.setItem("number-choice", number);
	redirect(location);
}

function cancelExam(location)
{
    localStorage.setItem("abandon",true); 
	redirect(location);
}

var theQuestion;
var bgs = [
'http://i.imgur.com/MjBOmGgg.jpg',
'http://i.imgur.com/mQA0fIOg.jpg',
'http://i.imgur.com/CsI5hAig.jpg',
'http://i.imgur.com/byzTecq.jpg',
'http://i.imgur.com/byzTecqg.jpg',
'http://i.imgur.com/pErfJrWg.jpg',
'http://i.imgur.com/d8bR96Bg.jpg',
'http://i.imgur.com/MArLNIXg.jpg'];
var im;
var repeat;
var globalExamQuestions;
var examIndex = 0;
var cExamArray = [];
function getBG(){
	return  bgs[Math.floor(Math.random()*bgs.length)];
}

window.onload= function(){
    //var baseHREF= window.location.pathname + window.location.search;
    //document.querySelector('a[href$="'+ baseHREF+ '"]').className += ' current';
    //im=document.getElementById('mainImage');
    //im.src = getBG();
    //repeat = setInterval(function() {
    //	changeImage();
    //}, 5000);

};

function changeImage(){
	var tmp = getBG();
	while (tmp == im.src){
		tmp = getBG();
	}
	im.src = tmp;
}

function getRandomQuestion(fCallback)
{
	$.ajax({
		url: "/ajax/randomQuestion",
		success: function(res){
			localStorage.setItem("QuestionObject", JSON.stringify(res));
			fCallback();
		}
	});
}

function getExamQuestions(fCallback, domain, number){

	$.ajax({
		url: "/ajax/examQuestions",
		data:{
			"domain": domain,
			"number": number
		},
		success: function(res){
			localStorage.setItem("ExamQuestions", JSON.stringify(res));
			fCallback();
		}
	});
}


var temp;
function LoadTestQuestion()
{
	$(".answerDiv label").attr("draggable",true);
	var object = JSON.parse(localStorage.getItem("QuestionObject"));
	temp = object;
	var question = object.question;
	$('#question').html(question);
	$('#domain').html(object.domaine);
	var $fLabel = $("label[for=answer-0]");
	var $sLabel = $("label[for=answer-1]");
	var $tLabel = $("label[for=answer-2]");
	$fLabel.attr("answerid","rep1").html(temp.reponse.rep1);
	$sLabel.attr("answerid","rep2").html(temp.reponse.rep2);
	$tLabel.attr("answerid","rep3").html(temp.reponse.rep3);

	$sLabel.on('dragstart', DragStart);
	$tLabel.on('dragstart', DragStart);
	$fLabel.on('dragstart', DragStart);

}


function LoadExamQuestion(index)
{
	$(".answerDiv label").attr("draggable",true);
	var questions = JSON.parse(localStorage.getItem("ExamQuestions"));
	var question = questions[index];
	$('#question').html(question.question);
	$('h1').html(question.domaine);
	var $fLabel = $("label[for=answer-0]");
	var $sLabel = $("label[for=answer-1]");
	var $tLabel = $("label[for=answer-2]");
	$fLabel.attr("answerid","rep1").html(question.reponse.rep1);
	$sLabel.attr("answerid","rep2").html(question.reponse.rep2);
	$tLabel.attr("answerid","rep3").html(question.reponse.rep3);

	$fLabel.on('dragstart', DragStart);
	$sLabel.on('dragstart', DragStart);
	$tLabel.on('dragstart', DragStart);

}

function DragStart (event) {
	var answerid = $(event.target).attr('answerid');
	// originalEvent is used because dataTransfer belongs to the HTML
	// event, not jQuery's. 
	event.originalEvent.dataTransfer.setData("answerid", answerid);
}

function VerifyAnswer(event, answerid){
	var questionObject = GetLocalStorageObject("QuestionObject");
	clearAnswersCSS();
	var $src = $(".answerDiv label[answerid=" + answerid + "]");
	var totalRandomExams = parseInt(localStorage.getItem("totalRandomExams")); // total random exams done
	totalRandomExams = isNaN(totalRandomExams)? 1 : totalRandomExams + 1;

	var totalRandomSucces = parseInt(localStorage.getItem('totalRandomSucces')); // total succesful random exams answered
	totalRandomSucces = isNaN(totalRandomSucces)? 0 : totalRandomSucces;
	localStorage.setItem("totalRandomExams",totalRandomExams);
	if (answerid == questionObject.bonneReponse){
		$src.closest("div").addClass("goodAnswer").removeClass("badAnswer");
		totalRandomSucces = totalRandomSucces + 1;
		localStorage.setItem("totalRandomSucces",totalRandomSucces);
	} else {
		$src.closest("div").addClass("badAnswer").removeClass("goodAnswer");
	}
	$(".answerDiv label").attr("draggable",false);
	UpdateStatistics();

}

function VerifyExamAnswer(event, answerid){
	var question = JSON.parse(localStorage.getItem("ExamQuestions"));
	var currentIndex = parseInt(localStorage.getItem("examIndex"));
	$(".answerDiv label").attr("draggable",false);
	return question[currentIndex].bonneReponse == answerid;
}

function NextQuestion(){
	clearAnswersCSS();
	$(".answerDiv input").attr("checked", false); // Remove the radio button selection
	getRandomQuestion(LoadTestQuestion);
}

function NextExamQuestion(){
	clearAnswersCSS();

	var idx = parseInt(localStorage.getItem("examIndex"));
	var questionNumbers = parseInt(localStorage.getItem("number-choice"));
	$(".answerDiv input").attr("checked", false); // Remove the radio button selection
	if(idx == questionNumbers-1){
		redirect('examscore');
	}
	else{
		localStorage.setItem("examIndex", idx + 1);
		LoadExamQuestion(idx + 1);
	}
}

function clearAnswersCSS(){
	$(".answerDiv div").removeClass("goodAnswer badAnswer");
}

function GetLocalStorageObject(key){
	return JSON.parse(localStorage.getItem(key));
}

function OnDrop(event){
	var value = event.dataTransfer.getData("answerid");	
	VerifyAnswer(event, value);
}

function OnDropExam(event){
	var value = event.dataTransfer.getData("answerid");	
	var isGoodAnswer = VerifyExamAnswer(event,value);
	var currentIndex = parseInt(localStorage.getItem("examIndex"));
	var $src = $(".answerDiv label[answerid=" + value + "]");

	if (isGoodAnswer){
		$src.closest("div").addClass("goodAnswer").removeClass("badAnswer");
		cExamArray[currentIndex] = 1;
	} else {
		cExamArray[currentIndex] = 0;
		$src.closest("div").addClass("badAnswer").removeClass("goodAnswer");
	}
	localStorage.setItem("examArray",JSON.stringify(cExamArray));
}

function allowDrop(event){
	event.preventDefault();
}

// Updates values of the mainTable
function UpdateStatistics(){
	var randSucces = localStorage.getItem("totalRandomSucces");
	randSucces = randSucces ? randSucces : "0";
	var randTries = localStorage.getItem("totalRandomExams");
	randTries = randTries ? randTries :"0";
	$("#randomStatsValue").html(randSucces + " / " + randTries);
}

function UpdateExamStats()
{
    var allResults = JSON.parse(localStorage.getItem("results"));
	$("#examBody").html("");
	$(allResults).each(function(index,item){
		var row ='<tr>';
		row+= ('<td> Exam( ' + item.dom + ' )</td>');
		row+= ('<td>' + item.score + " / " + item.totalValue + '</td>');
		row+='</tr>';
		$("#examBody").append(row);
	});
}

function UpdateExamAverage()
{
	var examAverage = localStorage.getItem("examAverage");
	if(examAverage == null){
		$("#examAverage").html( "ND");
	}
	else{
		$("#examAverage").html(examAverage + " %");
	}
    var allResults = JSON.parse(localStorage.getItem("results"));
	if(allResults == null){
		return;
	}
	var average;
	var total = allResults.length;
	var sum = 0;
	$(allResults).each(function(index,item){
		var percentage = item.score/item.totalValue*100;
		sum += percentage;
	});
	average = sum/total;
	$("#examAverage").html(average + "%");
	localStorage.setItem("examAverage",average);
}

// Deletes everything in localStorage
function ClearLocalStorage()
{
	localStorage.clear();
	UpdateStatistics();
}