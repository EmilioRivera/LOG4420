var questions =[
{
	"id": "1",
	"question": "What does HTML stand for?",
	"domaine": "HTML",
	"reponse": {
		"rep1": "Hyperlinks and Text Markup Language",
		"rep2": "Home Tool Markup Language",
		"rep3": "Hyper Tool Markup Language"
	},
	"bonneReponse": "rep3"
}, {
	"id": "2",
	"question": "Who is making the Web standards?",
	"domaine": "HTML",
	"reponse": {
		"rep1": "The World Wide Web Consortium",
		"rep2": "Google",
		"rep3": "There are no Web standards"
	},
	"bonneReponse": "rep3"
}, {
	"id": "3",
	"question": "What is the correct HTML element for inserting a line break?",
	"domaine": "HTML",
	"reponse": {
		"rep1": "lb",
		"rep2": "br",
		"rep3": "break"
	},
	"bonneReponse": "rep2"
}, {
	"id": "4",
	"question": " The external JavaScript file must contain the <script> tag.",
	"domaine": "Javascript",
	"reponse": {
		"rep1": "True",
		"rep2": "False",
		"rep3": "Banana"
	},
	"bonneReponse": "rep1"
}, {
	"id": "5",
	"question": "How do you call a function named myFunction ?",
	"domaine": "Javascript",
	"reponse": {
		"rep1": "myFunction()",
		"rep2": "call function myFunction()",
		"rep3": "call myFunction()"
	},
	"bonneReponse": "rep1"
}, {
	"id": "6",
	"question": "How does a WHILE loop start?",
	"domaine": "Javascript",
	"reponse": {
		"rep1": "while(i<=10)",
		"rep2": "while(i<=10;i++)",
		"rep3": "while i = 1 to 10"
	},
	"bonneReponse": "rep1"
}, {
	"id": "7",
	"question": "What does CSS stand for?",
	"domaine": "CSS",
	"reponse": {
		"rep1": "Computer Style Sheets",
		"rep2": "Cascading Style Sheets",
		"rep3": "Creative Style Sheets"
	},
	"bonneReponse": "Which is the correct CSS syntax?"
}, {
	"id": "8",
	"question": "How do you make the color black ?",
	"domaine": "CSS",
	"reponse": {
		"rep1": "{body;color:black;}",
		"rep2": "body:color=black;",
		"rep3": "{body:color=black;}"
	},
	"bonneReponse": "rep2"
}, {
	"id": "9",
	"question": "How do you make the text bold?",
	"domaine": "CSS",
	"reponse": {
		"rep1": "style: bold",
		"rep2": "font: bold",
		"rep3": "font-weight: bold"
	},
	"bonneReponse": "rep2"
}, {
	"id": "10",
	"question": "Which property is used to change the left margin of an element?",
	"domaine": "CSS",
	"reponse": {
		"rep1": "padding-left",
		"rep2": "margin-left",
		"rep3": "indent"
	},
	"bonneReponse": "rep3"
}, {
	"id": "11",
	"question": "How can you make a numbered list?",
	"domaine": "HTML",
	"reponse": {
		"rep1": "list",
		"rep2": "ol",
		"rep3": "ul"
	},
	"bonneReponse": "rep3"
}, {
	"id": "12",
	"question": "Inside which HTML element do we put the JavaScript?",
	"domaine": "Javascript",
	"reponse": {
		"rep1": "javascript",
		"rep2": "scripting",
		"rep3": "script"
	},
	"bonneReponse": "rep3"
}]


exports.GetRandomQuestion = function()
{
	var choice = questions[Math.floor(Math.random()*questions.length)];
	
	return choice;
};

exports.GetQuestionByDomain = function(domain,number){
	var results = questions.filter(function(obj){
		return obj.domaine == domain;
	});
	shuffle(results);
	number = (number <= results.length) ? number : results.length;
	results = results.slice(0,number);
	return results;
}

exports.GetQuestion = function(number){
	return number;
}
exports.TheVal = 23;

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}