var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var formController = require('../Controller/formcontroller');
var QuizCategories = require('../QuizCategories');
var mongoose = require( 'mongoose' );
var UserModel = mongoose.model('User');
var QuestionModel = mongoose.model('Question');
//var StatsModel = mongoose.model('Stats');

router.post('/question', jsonParser, function(req, res, next){
	if (!req.body) return res.sendStatus(400);
	var formatedQuestion = req.body.newQuestion;
	var theQuestion = formatedQuestion.question;
	console.log(req.body);
	var optionZero = formatedQuestion.answers[0];
	var optionOne = formatedQuestion.answers[1];
	var optionTwo = formatedQuestion.answers[2];
	console.log(optionZero);
	var category = formatedQuestion.domain;
	var optionArray = [];
	var answer_index = formatedQuestion.goodAnswer;
	var validCategories = QuizCategories.Categories;
	
	// Validate
	var isValid = true;
	if (theQuestion == "") { isValid = false ;}
	if (answer_index < 0 || answer_index > 2 || answer_index == null) { isValid = false;}
	if (!(validCategories.filter(function(cat){return cat.value === category;}).length > 0)) {isValid = false;}
	
	if (!isValid){
		res.sendStatus(400);
		return;
	}
	console.log("answer index: " + answer_index);
	
	optionArray.push(optionZero);
	optionArray.push(optionOne);
	optionArray.push(optionTwo);
	console.log(optionArray);
	// The client won't procede until a response is provided, that is
	// until a res is set.
	
	
	formController.CreateQuestion(theQuestion, category, optionArray, answer_index, function( err, product, count){
		if (err != null){
			res.sendStatus(new Error("Error while doing something"));
			return;
		}
		console.log("My three parameters : ");
		console.log("First parameter: " + err);
		console.log("Second parameter: " + product);
		console.log("Third parameter: " + count);
		console.log('All is ok, saved to db');
		res.json({status: "200", objectCreated: product});
	});
	
});

router.get('/question', function(req,res,next){
		var domain = req.query.domain;
		var count = req.query.count;
		if (count === undefined || isNaN(count)) count = 1;
		console.log('domain/count' + domain+'/'+count);
		console.log(req.query);
		formController.Questions(function(error,result){
			//console.log('Res for long thing');
			//console.log(result);
			
			var randomArray = [];
			var j = 0;
			while(j < count){
				var objectToAdd = result[Math.floor(Math.random()*result.length)];
				objectToAdd.bonneReponse = -1;
				randomArray.push(objectToAdd);
				j++;
			}
			res.json(randomArray);
		}, domain);
	});

router.get('/question/verify', function(req,res,next){
	var questionId = req.query.questionid;
	var chosenIndex = req.query.answerid;
	var isValid = true;
	if (questionId === undefined){isValid = false;}
	if (chosenIndex === undefined){isValid = false;}
	if (isValid == false){
		res.sendStatus(400);
		return;
	}
	
	//console.log('questionid is ' + questionId);
	//console.log('answerid is ' + chosenIndex);
	formController.VerifyQuestion(function(error, isGoodAnswer){
		if (error) { console.log(error);res.sendStatus(400); return;}
		//console.log('Verify answer: ' + isGoodAnswer);
		var us = UserModel.findOne({"user_name": "Harambe"},function(e,r){
			if (e) { console.log(e);res.sendStatus(400); return;}
			//console.log('r: ' + r);
			r.good_test += isGoodAnswer;
			r.total_test += 1;
			r.save(function(hE, hR){
				if (hE) { console.log(hE);}
				else { console.log('Success saving stats for ' + r.user_name)}
			});
			res.json(isGoodAnswer);
		});
	},questionId, chosenIndex);
});

router.get('/question/count', function(req,res,next){
	var domain = req.query.domain;
	if (domain !== undefined && !QuizCategories.exist(domain))
		{console.log('domain does not exist');res.sendStatus(400); return;}
	if (domain === undefined)
		{console.log('A domain is needed');res.sendStatus(400); return;}
	formController.Questions(function(error,result){
		if (error) { console.log(error);res.sendStatus(400); return;}
		var counts = {};
		QuizCategories.Categories.forEach(function(x){counts[x.value] = 0});
		result.forEach(function(x) { counts[x.domaine] = (counts[x.domaine] || 0)+1; });
		res.json(counts[domain]);
	}, domain);
	
});

router.post('/user', function(req,res,next){
	var username = (req.query.username === undefined ? "Harambe" : req.query.username);
	
	UserModel.findOne({"user_name": username}, function(error, result){
		if (error) { console.log(error);res.sendStatus(400); return;}
		if (result != null){ console.log('Already exists');res.sendStatus(418); return;}
		new UserModel({
			user_name: "Harambe",
			good_test: 0,
			total_test: 0,
			exam_in_progress: false,
			exam_index: 0,
			exams: []
		}).save(function(err, product, count){
			if (err) { console.log(err);res.sendStatus(400); return;}
			res.json(product);
		});
	});
});

router.get('/user', function(req,res,next){
	var userid = req.query.userid;
	if (userid !== undefined){
		UserModel.findById(userid, function(error, result){
			if (error) { console.log(error);res.sendStatus(400); return;}
			res.json(result);
		});
	} else {
		UserModel.findOne({"user_name": "Harambe"},function(e,r){
			if (e) { console.log(e);res.sendStatus(400); return;}
			res.json(r);
		});
	}
});

// This creates a new exam in the user profile if none has yet been started
// (and not finished)
router.get('/exam', function(req,res,next){
	var domain = req.query.domain;
	var count = req.query.count;
	var username = (req.query.username === undefined ? "Harambe" : req.query.username);
	// Validate
	var isValid = true;
	if (domain === undefined || !QuizCategories.exist(domain)) isValid = false;
	if (count === undefined || isNaN(count) || count <= 0) isValid = false;
	
	if (isValid === false) {
		console.log('NOPE');
		res.sendStatus(400);
		return;
	}
	
	// We have valid domain and number, get questions from DB
	QuestionModel.find({"domaine": domain}, function(error, result){
		if (error) { console.log(error);res.sendStatus(400); return;}
		if (result.length == 0) console.log("EMPTY SHIT");
		var questions = [];
		for(var i = 0; i < count; ++i){
			var pseudoRandom = Math.floor(Math.random()*result.length);
			questions.push(result[pseudoRandom]);
		}
		
		console.log('Array acquired : ' + questions);
		
		// Save the infomation to profile
		UserModel.findOne({"user_name": username},function(e,r){
			if (e) { console.log(e); res.sendStatus(400); return;}
			var examArray = [];
			console.log('Using user ' + r.user_name);
			console.log(r);
			if (r.exam_in_progress){
				// TODO : Right Code
				console.log('An exam has already been started');
				res.sendStatus(400);
				return;
			}
			questions.forEach(function(x){
				console.log('Adding ' + x._id);
				examArray.push({
					"question" : x._id.toString(),
					"score": -1,
					"domain": domain
				});
			});
			r.exam_in_progress = true;
			r.exam_index = 0;
			console.log('Array inserted : ' + examArray);
			//if (r.exams === undefined) r.exams = [];
			r.exams.push(examArray);
			r.save(function(hE,hR){
				console.log('Exam started for ' + r.user_name );
				res.json(questions);
			});
			
		});
		
	});
});
/* Same thing as ^^^ ^^^^^, but with render */
router.get('/exampage', function(req,res,next){
	var username = (req.query.username === undefined ? "Harambe" : req.query.username);
	// Validate
	var isValid = true;
	if (isValid === false) {
		console.log('NOPE');
		res.sendStatus(400);
		return;
	}

	// Save the infomation to profile
	UserModel.findOne({"user_name": username},function(e,r){
		if (e) { console.log(e); res.sendStatus(400); return;}
		var examArray = [];
		console.log('Using user ' + r.user_name);
		console.log(r);
		if (!r.exam_in_progress){
			// TODO : Right Code
			console.log('No exam has been started');
			res.sendStatus(400);
			return;
		}
		var currentExam = r.exams[r.exams.length - 1];
		var currentIndex = r.exam_index;
		var currentQ = currentExam[currentIndex];
		//console.log('currentIndex|currentExam.length : ' + currentIndex + '|' + currentExam.length);
		if (currentIndex >= currentExam.length){
			
			r.exam_index = 0;
			r.exam_in_progress = false;
			var newUserAgain = new UserModel(r);
			//console.log('New user again : ' + newUserAgain);
			newUserAgain.save(function(x){
				if (x) { console.log(x); res.sendStatus(400); return;}
				redirectToScore(req,res,next,currentExam);
			});	
			return;
		}
		console.log('currentQ : ' + currentQ);
		var cExam = currentExam.map(function(s){return new mongoose.Types.ObjectId(s.question);});;
		console.log('cExam : ' + cExam);
		//QuestionModel.find({"_id" : {"$in" : cExam}}).exec(function(a,b){
		QuestionModel.findById(currentQ.question,function(a,b){
			console.log(a);
			console.log('as');
			console.log(b);
			
			res.render('exampage.pug',{question: b, index: currentIndex});
			return;
		});
		
	});	
});

function redirectToScore(req,res,next,exam){
	try{
		var total = exam.length;
		var score = exam.reduce(function(s,z){return s+z.score},0);
		var percentage = score / total;
		res.render('examscore.pug', {score: score, total: total, percentage: percentage});
		return;
	} catch (e){
		res.render('error.pug', {err: e});
		return;
	}
}

// Gets the question at which the user is currently at
//router.get('/exam/question', function(req,res,next){
//	var username = (req.query.username === undefined ? "Harambe" : req.query.username);
//	// Validate
//	var isValid = true;
//	if (isValid === false) {
//		console.log('NOPE');
//		res.sendStatus(400);
//		return;
//	}
//	UserModel.findOne({"user_name": username},function(e,r){
//		if (e) { console.log(e); res.sendStatus(400); return; }
//		if (!r.exam_in_progress){
//			// TODO : Right Code
//			console.log('No exam has been started');
//			res.sendStatus(400);
//			return;
//		}
//		var currentExam = r.exams[r.exams.length - 1];
//		var currentIndex = r.exam_index;
//		var currentQ = currentExam[currentIndex];
//		QuestionModel.findById(currentQ.question,function(a,b){
//			if (a) { console.log(a); res.sendStatus(400); return;}
//			
//			// User has completed all questions
//			if (currentIndex >= currentExam.length){
//				console.log('User has completed all questions');
//				redirectToScore(req,res,next,currentExam);
//				return;
//			} else {
//				// TODO : DO
//				res.json(b);
//			}
//		});
//		
//	});
//		
//});

router.post('/exam/verify', function(req,res,next){
	
	var choice = req.body.choice;
	var username = (req.query.username === undefined ? "Harambe" : req.query.username);
	console.log('Choice : ' + choice);
	var isValid = true;
	if (choice === undefined || isNaN(choice) || choice < 0){ isValid = false; }
	if(isValid === false){
		console.log('Choice invalid :' + choice);
		res.sendStatus(400);
		return;
	}
	
	UserModel.findOne({"user_name": username}, function(e,r){
		if (e) { console.log(e); res.sendStatus(400); return;}
		console.log(r);
		if (!r.exam_in_progress){
			console.log('User has not started an exam');
			res.sendStatus(400);
			return;
		}
		
		var currentExam = r.exams[r.exams.length - 1];
		var currentIndex = r.exam_index;
		var currentQ = currentExam[currentIndex];
		// If the choice is OK, we get associated question and 
		// check its answer
		QuestionModel.findById(currentQ.question,function(a,b){
			if (a) { console.log(a); res.sendStatus(400); return;}
			
			var optionsCount = b.reponse.length;
			
			if (choice >= optionsCount){
				res.sendStatus(400);
				return;
			}
			console.log('Good answer is : ' + b.bonneReponse);
			var isGoodAnswer = false;
			
			// Notice that we only use 2 '=' so that a cast is made from the string
			if (b.bonneReponse == choice){
				console.log('User has good answer !');
				currentQ.score = 1;
				isGoodAnswer = true;
			} else {
				currentQ.score = 0;
				console.log('User failed : bad answer !');
			}
			// TODO : Increment the index of the user
			r.exam_index += 1;
			
			// User has finished
			if (currentIndex >= currentExam.length){
				r.exam_in_progress = false;
			}
			var newUser = new UserModel(r);
			// Save progress
			newUser.save(function(x){
				console.log(x);
				console.log('Saving exam progress : ' + x);
				res.json(isGoodAnswer);	
				return;
			});
			
		});
		
	});
	
});

router.delete('/question', function(req,res,next){

	QuestionModel.remove(function(error, removed){
		if (error) { console.log(error);res.sendStatus(400); return;}
		res.json(removed);
	});
});

module.exports = router;
