"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var question_service_1 = require('./question.service');
var QuestionComponent = (function () {
    function QuestionComponent(questionService) {
        this.questionService = questionService;
        this.value = "Hello";
    }
    QuestionComponent.prototype.ngOnInit = function () {
        this.getRandomQuestion();
        this.hasChosen = false;
    };
    QuestionComponent.prototype.getRandomQuestion = function (category) {
        var _this = this;
        // Call the service
        this.questionService.getRandomQuestion(category)
            .then(function (theQuestion) {
            return _this.currentQuestion = theQuestion;
        });
    };
    QuestionComponent.prototype.handle = function (x) {
        if (x) {
            console.log("YEAH NIGGAH");
        }
        else {
            console.log("YOU DIED");
        }
    };
    QuestionComponent.prototype.drop = function (event) {
        if (this.hasChosen) {
            return;
        }
        var value = event.dataTransfer.getData("answerid");
        // Call the thing
        this.questionService.verifyAnswer(this.currentQuestion.id, value)
            .then(this.handle);
        this.hasChosen = true;
    };
    QuestionComponent.prototype.dragover = function (event) {
        if (event.preventDefault) {
            event.preventDefault();
        }
    };
    QuestionComponent.prototype.dragstart = function (event) {
        var answerid;
        for (var i = 0, atts = event.target.attributes, n = atts.length, arr = []; i < n; i++) {
            if (atts[i].nodeName == "answerid") {
                answerid = atts[i].nodeValue;
                break;
            }
        }
        console.log(answerid);
        event.dataTransfer.setData("answerid", answerid);
    };
    QuestionComponent.prototype.allowDrop = function (event) {
        event.preventDefault();
    };
    QuestionComponent = __decorate([
        core_1.Component({
            selector: 'my-question',
            templateUrl: "/templates/quicktest"
        }), 
        __metadata('design:paramtypes', [question_service_1.QuestionService])
    ], QuestionComponent);
    return QuestionComponent;
}());
exports.QuestionComponent = QuestionComponent;
//# sourceMappingURL=question.component.js.map