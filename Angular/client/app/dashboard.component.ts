import { Component, OnInit } from '@angular/core';
import { QuestionService } from './question.service';
import { Domain } from './domain';
import { DOMAINS } from './valid-domains'

@Component({
  selector: 'my-dashboard',
  templateUrl: "templates/dashboard"
})
export class DashboardComponent implements OnInit{
	
	private selectedDomain : string;
	private maxValue : number;
	private allDomains: Domain[];

	constructor(private questionService : QuestionService){

	}


	ngOnInit() : void {
		this.allDomains = DOMAINS;
		this.maxValue = 10;
		this.getQuestionCount("js");
	}

	private getQuestionCount(selection: any){
		let s: string = selection.toString();
		let ds: Array<Domain>  = this.allDomains.filter(d =>  d.value == s);
		if (ds.length == 0){
			console.log("Not a valid domain, returning");
			return;
		}
		//this.selectedDomain =  selection.toString();
		this.questionService.getQuestionCount(selection.toString())
			.then(x => this.maxValue = x);
	}

	private updateCount(count : any){
		// TODO
		this.maxValue = Number.parseInt(count);
	}
 }
