"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var question_service_1 = require('./question.service');
var question_1 = require('./question');
var valid_domains_1 = require('./valid-domains');
var CreateQuestionComponent = (function () {
    function CreateQuestionComponent(questionService) {
        this.questionService = questionService;
    }
    CreateQuestionComponent.prototype.createQuestion = function () {
        var _this = this;
        console.log(this.question.domain);
        for (var _i = 0, _a = this.question.answers; _i < _a.length; _i++) {
            var answer = _a[_i];
            console.log(answer);
        }
        console.log(this.question.goodAnswer);
        this.questionService.createQuestion(this.question)
            .then(function (x) { return _this.showQuestion(x); })
            .then(function (x) { return _this.InitializeQuestion(); });
    };
    CreateQuestionComponent.prototype.deleteAllQuestions = function () {
        var _this = this;
        this.questionService.deleteAllQuestions()
            .then(function (x) { return _this.showDeletedQuestion(x); });
    };
    CreateQuestionComponent.prototype.ngOnInit = function () {
        this.allDomains = valid_domains_1.DOMAINS;
        this.choices = new Array(3);
        this.InitializeQuestion();
    };
    CreateQuestionComponent.prototype.InitializeQuestion = function () {
        this.question = new question_1.Question();
        this.question.question = "";
        this.question.goodAnswer = 0;
        this.question.answers = new Array(3);
        this.question.domain = "js";
    };
    CreateQuestionComponent.prototype.showQuestion = function (q) {
        console.log(q);
        alert("Question Cree");
    };
    CreateQuestionComponent.prototype.showDeletedQuestion = function (q) {
        console.log(q);
        alert("Toutes les questions sont supprimees");
    };
    CreateQuestionComponent = __decorate([
        core_1.Component({
            selector: 'my-create-question',
            template: "\n  \t<section *ngIf=\"question\">\n  \t\t<select #catSelect [(ngModel)]=\"question.domain\">\n  \t\t\t<option *ngFor=\"let domain of allDomains\" value={{domain.value}} text={{domain.fullName}}>\n  \t\t</select>\n  \t\t<input #questionName placeholder=\"Question goes here\" \n  \t\ttype=\"text\" [(ngModel)]=\"question.question\">\n  \t\t<div *ngFor=\"let choice of choices;let i = index\">\n  \t\t\t<div class =\"answerGroup\">\n  \t\t\t\t<input type='radio' #radio{{i}} name='answerGroup' (click)=\"question.goodAnswer = i\" [checked]=\"i===question.goodAnswer\">\n  \t\t\t\t<input type='text' #text{{i}} [(ngModel)] = \"question.answers[i]\">\n  \t\t\t</div>\n  \t\t</div>\n  \t\t<div>\n  \t\t\t<button #submitButtonn (click)=\"createQuestion()\"> Submit\n  \t\t\t</button>\n  \t\t</div>\n  \t\t<button #delete_questions_btn type='button' (click)=\"deleteAllQuestions()\">Delete All Questions!\n  \t\t</button>\n  \t</section>\n  "
        }), 
        __metadata('design:paramtypes', [question_service_1.QuestionService])
    ], CreateQuestionComponent);
    return CreateQuestionComponent;
}());
exports.CreateQuestionComponent = CreateQuestionComponent;
//# sourceMappingURL=createquestion.component.js.map