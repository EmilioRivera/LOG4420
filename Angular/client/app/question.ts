export class Question{
	id: string;
    question: string;
    domain: string;
    answers: string[];
    goodAnswer: number;
}