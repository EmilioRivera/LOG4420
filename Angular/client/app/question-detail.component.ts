import {Component, Input } from '@angular/core';

import { Question } from './question';

@Component({
    selector: 'my-question-detail',
    template: `
        <div *ngIf="question">
            <h2> {{question.domain}}</h2>
            <h3> {{question.question}}</h3>
        </div>
    `
})

export class QuestionDetailComponent{
    @Input()
    question: Question;
}