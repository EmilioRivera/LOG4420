"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var dashboard_component_1 = require('./dashboard.component');
var question_component_1 = require('./question.component');
var instructions_component_1 = require('./instructions.component');
var acceuil_component_1 = require('./acceuil.component');
var question_detail_component_1 = require('./question-detail.component');
var createquestion_component_1 = require('./createquestion.component');
// TODO : Put the right paths
var routes = [
    { path: '', redirectTo: '/accueil', pathMatch: 'full' },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent },
    { path: 'accueil', component: acceuil_component_1.AcceuilComponent },
    { path: 'instructions', component: instructions_component_1.InstructionsComponent },
    { path: 'index/:id', component: question_detail_component_1.QuestionDetailComponent },
    { path: 'exam', component: question_component_1.QuestionComponent },
    { path: 'quicktest', component: question_component_1.QuestionComponent },
    { path: 'createquestion', component: createquestion_component_1.CreateQuestionComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map