import { NgModule  }      from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { FormsModule  }   from '@angular/forms';
import { RouterModule  }   from '@angular/router';
import { AppRoutingModule } from './app-routing.module'
import { HttpModule }    from '@angular/http';

import { AppComponent } from './app.component';
import { QuestionComponent } from './question.component';
import { InstructionsComponent } from './instructions.component';
import { QuestionDetailComponent } from './question-detail.component';
import { CreateQuestionComponent } from './createquestion.component';
import { DashboardComponent } from './dashboard.component';
import { AcceuilComponent } from './acceuil.component';
import { StatsComponent } from './user-stats.component';

import { QuestionService } from './question.service';
import { UserStatsService } from './user-stats.service';

@NgModule({
  imports: [
    BrowserModule, 
    FormsModule,
    HttpModule, 
    AppRoutingModule
  ],
  declarations: [ AppComponent, QuestionDetailComponent, QuestionComponent, DashboardComponent, AcceuilComponent, InstructionsComponent, StatsComponent, CreateQuestionComponent],
  providers: [ QuestionService, UserStatsService ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
