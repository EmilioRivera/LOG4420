"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var user_stats_1 = require('./user-stats');
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/map');
var UserStatsService = (function () {
    function UserStatsService(http) {
        this.http = http;
        this.apiUrlStats = '/api/user';
    }
    UserStatsService.prototype.getStats = function (user) {
        var params = new http_1.URLSearchParams();
        var uid = 1337;
        params.set("userid", uid.toString());
        return this.http.get(this.apiUrlStats)
            .map(this.extractStats)
            .toPromise()
            .catch(this.handleError);
    };
    UserStatsService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    UserStatsService.prototype.extractStats = function (r) {
        var rjson = r.json();
        console.log(rjson);
        var uStats = new user_stats_1.UserStats();
        // TODO : Put the right values
        uStats.user_name = "Harambe";
        uStats.good_test = rjson.good_test;
        uStats.total_test = rjson.total_test;
        uStats.exam_in_progress = rjson.exam_in_progress;
        uStats.exam_index = rjson.exam_index;
        uStats.exams = rjson.exams;
        /*uStats.exams =  [
            [
                { question_id : 1, score: 0, domain: "css"},
                { question_id : 2, score: 1, domain: "js"},
                { question_id : 3, score: 0, domain: "html"}
            ]
        ];*/
        var sum = 0;
        for (var _i = 0, _a = uStats.exams; _i < _a.length; _i++) {
            var exam = _a[_i];
            var sub = 0;
            for (var _b = 0, exam_1 = exam; _b < exam_1.length; _b++) {
                var q = exam_1[_b];
                sub += q.score;
            }
            sum += (sub / exam.length);
        }
        var t = 100 * sum / uStats.exams.length;
        uStats.average = t.toPrecision(4).toString();
        return uStats;
    };
    UserStatsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], UserStatsService);
    return UserStatsService;
}());
exports.UserStatsService = UserStatsService;
//# sourceMappingURL=user-stats.service.js.map