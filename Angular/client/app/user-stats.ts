// import { Question } from './question'; // If we want to save the whole questions

export class UserStats{
	user_name: string;
	good_test: number;
	total_test: number;
	average : string;
    exam_in_progress: Boolean;
    exam_index: number;
	exams: [[{
		question_id: number,
		score: number,
		domain: string
	}]];
};