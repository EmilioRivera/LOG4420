import { Domain } from './domain';

export const DOMAINS: Domain[] = [
	{value: 'js', fullName : 'JavaScript'},
	{value: 'css', fullName : 'CSS'},
	{value: 'html', fullName : 'HTML'}
];