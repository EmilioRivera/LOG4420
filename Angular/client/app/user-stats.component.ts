import { Component, OnInit } from '@angular/core'
import { UserStatsService } from './user-stats.service';
import { UserStats } from './user-stats';

@Component({
	selector: 'my-stats-component',
    templateUrl: "/templates/stats"
})

export class StatsComponent implements OnInit{
    title = "Harambe's Quiz";
    private stats : UserStats;

    constructor(private userStatsService : UserStatsService){}

    ngOnInit():void
    {
    	this.getUserStatistics();
    }

    getUserStatistics(): void{
    	this.userStatsService.getStats()
    		.then(s => this.stats = s);

    }

    examAverage(ex: any): string{
        //console.log(ex);
        let total: number = 0;
        for(let q of ex){
            total += q.score;
        }
        let s: string = total.toString() + '/' + ex.length;
        return s;
    }
}