"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var question_service_1 = require('./question.service');
var valid_domains_1 = require('./valid-domains');
var DashboardComponent = (function () {
    function DashboardComponent(questionService) {
        this.questionService = questionService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.allDomains = valid_domains_1.DOMAINS;
        this.maxValue = 10;
        this.getQuestionCount("js");
    };
    DashboardComponent.prototype.getQuestionCount = function (selection) {
        var _this = this;
        var s = selection.toString();
        var ds = this.allDomains.filter(function (d) { return d.value == s; });
        if (ds.length == 0) {
            console.log("Not a valid domain, returning");
            return;
        }
        //this.selectedDomain =  selection.toString();
        this.questionService.getQuestionCount(selection.toString())
            .then(function (x) { return _this.maxValue = x; });
    };
    DashboardComponent.prototype.updateCount = function (count) {
        // TODO
        this.maxValue = Number.parseInt(count);
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'my-dashboard',
            templateUrl: "templates/dashboard"
        }), 
        __metadata('design:paramtypes', [question_service_1.QuestionService])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map