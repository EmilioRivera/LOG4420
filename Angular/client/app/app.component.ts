import { Component } from '@angular/core'

@Component({
    selector: 'my-app',
    templateUrl: "/templates/navigation",
    styleUrls: ['/stylesheets/style.css']
})

export class AppComponent{
    title = "Harambe's Quiz";
}