import { Injectable } from '@angular/core';
import { Question } from './question';
import { Headers, Http, URLSearchParams, RequestOptionsArgs, Response} from '@angular/http'
import { DOMAINS } from './valid-domains';
import { Domain } from './domain';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
@Injectable()
export class QuestionService{

    private apiUrlQuestion = '/api/question';
    private apiUrlVerification = '/api/question/verify';
    private apiUrlQuestionCount = '/api/question/count';
    private apiUrl = '/api/';
    constructor(private http: Http){}

    getRandomQuestion(category? : string): Promise<Question> {
        
        let res : string  = category == undefined ? "randomCat" : category;
        let params : URLSearchParams = new URLSearchParams();
        // TODO : Put the right domain and count and stuff....
        params.set("domain", "js");
        params.set("count", "1");
        return this.http.get(this.apiUrlQuestion,{
                search: params
            })
            .map(this.extractOneQuestion)
            .toPromise()
            .catch(this.handleError);
    }

    verifyAnswer(id: string, answerid: number) : Promise<Boolean>{
        
        let params : URLSearchParams = new URLSearchParams();
        
        params.set("questionid", id);
        params.set("answerid", answerid.toString());

        return this.http.get(this.apiUrlVerification,{
                    search: params
                }).map(this.verificationParsing)
                .toPromise()
                .catch(this.handleError);
    }

    getQuestionCount(domain?: string) : Promise<any>{

        let params : URLSearchParams = new URLSearchParams();
        params.set("domain", domain);
        console.log("domain passed is : " + domain);
        return this.http.get(this.apiUrlQuestionCount,{
                search: params
                })
                .map(this.extractCount)
                .toPromise()
                .catch(this.handleError);
    }

    createQuestion(newQuestion : Question) : Promise<any>{
        let params : URLSearchParams = new URLSearchParams();
        params.set("question",newQuestion.toString());
        return this.http.post(this.apiUrlQuestion,{
            newQuestion
            })
            .map(this.extractNewQuestion)
            .toPromise()
            .catch(this.handleError);

    }

    deleteAllQuestions(): Promise<any>{
        return this.http.delete(this.apiUrlQuestion)
                .map(this.extractDeletedQuestion)
                .toPromise()
                .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    private extractDeletedQuestion(r:Response){
        return r.json();
    }
    
    private extractNewQuestion(r:Response){
        return r.json();
    }

    private extractOneQuestion(r: Response){
        let rjson = r.json()[0];
        console.log(rjson);
        let q: Question = new Question();
        // Some names are in French.... 
        //let domain: Domain = DOMAINS.filter(d => return d.value == rjson.domaine)[0];
        console.log(DOMAINS);
        q.domain = "Javascript";//domain.fullName;
        q.question = rjson.question;
        q.answers = rjson.reponse;
        q.id = rjson._id;
        return q;
    }

    private verificationParsing(r : Response){
        return r.json();
    }

    private extractCount(r : Response){
        console.log(r.json());
        return r.json();
    }
}
