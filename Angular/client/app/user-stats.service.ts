import { Injectable } from '@angular/core';
import { Question } from './question';
import { Headers, Http, URLSearchParams, RequestOptionsArgs, Response} from '@angular/http'
import { DOMAINS } from './valid-domains';
import { Domain } from './domain';
import { UserStats } from './user-stats';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class UserStatsService{

    private apiUrlStats = '/api/user';
    constructor(private http: Http){}

    getStats(user? : string): Promise<UserStats> {        
        let params : URLSearchParams = new URLSearchParams();
        let uid : number = 1337;
        params.set("userid",  uid.toString());
        return this.http.get(this.apiUrlStats)
                .map(this.extractStats)
                .toPromise()
                .catch(this.handleError);
        
        
       
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    
    private extractStats(r: Response){
        
        let rjson = r.json();
        console.log(rjson);
        let uStats: UserStats = new UserStats();
        // TODO : Put the right values
        uStats.user_name =  "Harambe";
        uStats.good_test =  rjson.good_test;
        uStats.total_test =  rjson.total_test;
        uStats.exam_in_progress = rjson.exam_in_progress;
        uStats.exam_index =  rjson.exam_index;
        uStats.exams = rjson.exams;
        /*uStats.exams =  [
            [
                { question_id : 1, score: 0, domain: "css"},
                { question_id : 2, score: 1, domain: "js"},
                { question_id : 3, score: 0, domain: "html"} 
            ]
        ];*/

        let sum: number = 0;
        for(let exam of uStats.exams){
            let sub : number = 0;
            for(let q of exam){
                sub += q.score;
            }
            sum += (sub / exam.length);
        }
        let t: number  = 100 * sum / uStats.exams.length;
        uStats.average = t.toPrecision(4).toString();

        return uStats;
    }
}
