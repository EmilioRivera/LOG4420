"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var question_1 = require('./question');
var http_1 = require('@angular/http');
var valid_domains_1 = require('./valid-domains');
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/map');
var QuestionService = (function () {
    function QuestionService(http) {
        this.http = http;
        this.apiUrlQuestion = '/api/question';
        this.apiUrlVerification = '/api/question/verify';
        this.apiUrlQuestionCount = '/api/question/count';
        this.apiUrl = '/api/';
    }
    QuestionService.prototype.getRandomQuestion = function (category) {
        var res = category == undefined ? "randomCat" : category;
        var params = new http_1.URLSearchParams();
        // TODO : Put the right domain and count and stuff....
        params.set("domain", "js");
        params.set("count", "1");
        return this.http.get(this.apiUrlQuestion, {
            search: params
        })
            .map(this.extractOneQuestion)
            .toPromise()
            .catch(this.handleError);
    };
    QuestionService.prototype.verifyAnswer = function (id, answerid) {
        var params = new http_1.URLSearchParams();
        params.set("questionid", id);
        params.set("answerid", answerid.toString());
        return this.http.get(this.apiUrlVerification, {
            search: params
        }).map(this.verificationParsing)
            .toPromise()
            .catch(this.handleError);
    };
    QuestionService.prototype.getQuestionCount = function (domain) {
        var params = new http_1.URLSearchParams();
        params.set("domain", domain);
        console.log("domain passed is : " + domain);
        return this.http.get(this.apiUrlQuestionCount, {
            search: params
        })
            .map(this.extractCount)
            .toPromise()
            .catch(this.handleError);
    };
    QuestionService.prototype.createQuestion = function (newQuestion) {
        var params = new http_1.URLSearchParams();
        params.set("question", newQuestion.toString());
        return this.http.post(this.apiUrlQuestion, {
            newQuestion: newQuestion
        })
            .map(this.extractNewQuestion)
            .toPromise()
            .catch(this.handleError);
    };
    QuestionService.prototype.deleteAllQuestions = function () {
        return this.http.delete(this.apiUrlQuestion)
            .map(this.extractDeletedQuestion)
            .toPromise()
            .catch(this.handleError);
    };
    QuestionService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    QuestionService.prototype.extractDeletedQuestion = function (r) {
        return r.json();
    };
    QuestionService.prototype.extractNewQuestion = function (r) {
        return r.json();
    };
    QuestionService.prototype.extractOneQuestion = function (r) {
        var rjson = r.json()[0];
        console.log(rjson);
        var q = new question_1.Question();
        // Some names are in French.... 
        //let domain: Domain = DOMAINS.filter(d => return d.value == rjson.domaine)[0];
        console.log(valid_domains_1.DOMAINS);
        q.domain = "Javascript"; //domain.fullName;
        q.question = rjson.question;
        q.answers = rjson.reponse;
        q.id = rjson._id;
        return q;
    };
    QuestionService.prototype.verificationParsing = function (r) {
        return r.json();
    };
    QuestionService.prototype.extractCount = function (r) {
        console.log(r.json());
        return r.json();
    };
    QuestionService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], QuestionService);
    return QuestionService;
}());
exports.QuestionService = QuestionService;
//# sourceMappingURL=question.service.js.map