import { Component, OnInit } from '@angular/core';
import { QuestionService } from './question.service';
import { Question } from './question';
import { Domain } from './domain';
import { DOMAINS } from './valid-domains';

@Component({
  selector: 'my-create-question',
  template:`
  	<section *ngIf="question">
  		<select #catSelect [(ngModel)]="question.domain">
  			<option *ngFor="let domain of allDomains" value={{domain.value}} text={{domain.fullName}}>
  		</select>
  		<input #questionName placeholder="Question goes here" 
  		type="text" [(ngModel)]="question.question">
  		<div *ngFor="let choice of choices;let i = index">
  			<div class ="answerGroup">
  				<input type='radio' #radio{{i}} name='answerGroup' (click)="question.goodAnswer = i" [checked]="i===question.goodAnswer">
  				<input type='text' #text{{i}} [(ngModel)] = "question.answers[i]">
  			</div>
  		</div>
  		<div>
  			<button #submitButtonn (click)="createQuestion()"> Submit
  			</button>
  		</div>
  		<button #delete_questions_btn type='button' (click)="deleteAllQuestions()">Delete All Questions!
  		</button>
  	</section>
  `
  //templateUrl: "templates/createquestion"
})
export class CreateQuestionComponent implements OnInit{
	allDomains : Domain[];
	question : Question;
	choices : Array<string>;
	constructor(private questionService : QuestionService){
	}

	createQuestion(){
		console.log(this.question.domain);
		for(let answer of this.question.answers){
			console.log(answer);
		}
		console.log(this.question.goodAnswer);

		this.questionService.createQuestion(this.question)
		.then(x => this.showQuestion(x))
		.then(x => this.InitializeQuestion());
	}


	private deleteAllQuestions() : void	{
		this.questionService.deleteAllQuestions()
			.then(x=>this.showDeletedQuestion(x));
	}

	ngOnInit() : void {
		this.allDomains = DOMAINS;
		this.choices = new Array<string>(3);
		this.InitializeQuestion();

	}
	private InitializeQuestion():void{
		this.question = new Question();
		this.question.question = "";
		this.question.goodAnswer = 0;
		this.question.answers = new Array<string>(3);
		this.question.domain = "js";
	}

	private showQuestion(q:any) : void{
		console.log(q);
		alert("Question Cree");
	}
	private showDeletedQuestion(q:any) : void{
		console.log(q);
		alert("Toutes les questions sont supprimees");
	}
 }
