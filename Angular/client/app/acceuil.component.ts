import { Component } from '@angular/core'

@Component({
    templateUrl: "/templates/acceuil",
    styleUrls: ['/stylesheets/style.css']
})

export class AcceuilComponent{
    title = "Harambe's Quiz";
}