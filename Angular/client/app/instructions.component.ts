import { Component } from '@angular/core'

@Component({
    selector: 'my-app',
    templateUrl: "/templates/instructions",
    styleUrls: ['/stylesheets/style.css']
})

export class InstructionsComponent{
    title = "Harambe's Quiz";
}