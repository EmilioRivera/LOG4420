import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from './dashboard.component';
import { QuestionComponent } from './question.component';
import { InstructionsComponent } from './instructions.component';
import { AcceuilComponent } from './acceuil.component';
import { QuestionDetailComponent } from './question-detail.component';
import { CreateQuestionComponent } from './createquestion.component';

// TODO : Put the right paths
const routes: Routes = [
  { path: '', redirectTo: '/accueil', pathMatch: 'full' },
  { path: 'dashboard',  component: DashboardComponent },
  { path: 'accueil',  component: AcceuilComponent },
  { path: 'instructions',  component: InstructionsComponent },
  { path: 'index/:id', component: QuestionDetailComponent },
  { path: 'exam',     component: QuestionComponent },
  { path: 'quicktest', component: QuestionComponent },
  { path: 'createquestion', component: CreateQuestionComponent }


];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}