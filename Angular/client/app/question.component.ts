import { Component, OnInit  } from '@angular/core';
import { Question } from './question';
import { QuestionService } from './question.service';

@Component({
  selector: 'my-question',
  templateUrl: "/templates/quicktest"
})

export class QuestionComponent implements OnInit {
  private value = "Hello";
  bidonQuestions: Question[];
  currentQuestion : Question;
  private hasChosen: Boolean;
  constructor(private questionService : QuestionService){ }

  ngOnInit(): void {
    this.getRandomQuestion();
    this.hasChosen = false;
  }
  getRandomQuestion(category? : string) : void{
    // Call the service
    this.questionService.getRandomQuestion(category)
      .then(theQuestion =>
        this.currentQuestion  = theQuestion
      );
  }
  private handle(x: Boolean){
    if (x){
      console.log("YEAH NIGGAH");
    } else {
      console.log("YOU DIED");
    }
  }
  drop(event): void{
    if (this.hasChosen){
      return;
    }
    var value = event.dataTransfer.getData("answerid");
    // Call the thing
    this.questionService.verifyAnswer(this.currentQuestion.id, value)
      .then(this.handle);
    this.hasChosen = true;
  }
  dragover(event): void{
    if (event.preventDefault){
      event.preventDefault();
    }
  }
  dragstart(event){
    var answerid;
    for (var i = 0, atts = event.target.attributes, n = atts.length, arr = []; i < n; i++){
      if (atts[i].nodeName == "answerid"){
        answerid = atts[i].nodeValue;
        break;
      }
    }
    console.log(answerid);
    event.dataTransfer.setData("answerid", answerid);
  }

  allowDrop(event){
    event.preventDefault();
  }

}
